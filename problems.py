#!/usr/bin/python

#Author:	Andreas Joss (16450612)
#Date:		2015/04/14 

from __future__ import division				#ensures division always returns the correct answer (eg. 1/2 = 0.5 instead of 1/2 = 0)
from sympy import *
import pylab as pl
import sys 

r = 0.618034						#golden ratio

def printErr():
  print "Function definition outside the class works."

# Obtain absolute value of a vector
def getAbsoluteOfVector(vector):					
  sum = 0
  n_dim = pl.np.size(vector)
  
  for i in range (0,n_dim):
    sum = sum + pl.np.power(vector[i],2)
  
  return sum**Rational(1,2)			#must use sympy's rational function because sum contains a sympy_float object type


class problem:
  """A class that contains all the data for every example problem."""		
  
  #The docstring which belongs to this class.
  
  def __init__(self,name,n_dim):
    self.name = name
    self.n_dim = n_dim
    
    self.x = []			#creates a new empty list for every problem (this specific list will be used for Sympy symbols)
    self.x_val = []
    self.x_val.append([])
    self.f_val = []
    self.f_val_outer = []
    self.feasible_graph = []
    self.theoretical_solution = []
    self.start = []
    self.gradient_vector = []
    self.gradient_vector_at_point = []
    self.gradient_vector_at_point_previous = []
    self.absolute_of_first_order_gradient_vector = None
    self.g = []	#inequality constraints
    self.g_boolean = []
    self.h = []	#equality constraints
    self.h_boolean = []
    self.tmp_pointer = None
    self.uncomplete_x_val = []
    self.penalties = False
    self.do_sumt = False
    self.do_al = False

    #create dual space variables (Langrangian variables)
    self.lambda_eq = []
    self.lambda_ineq = []
    
    for i in range (0, self.n_dim):
      self.x_val[0].append(0)	
      self.theoretical_solution.append(0)
      #self.start.append(0)
      self.gradient_vector.append(0)
      self.gradient_vector_at_point.append(0)
      self.gradient_vector_at_point_previous.append(0)
   
    
    self.original_function = ""
    self.function = ""
    self.epsilon1 = 1e-4			#10^(-4)	1st termination condition of steepest descent (outer loop)
    self.epsilon2 = 1e-4			#10^(-4)	2nd termination condition of steepest descent (outer loop)
    self.epsilon_gs = 1e-6			#10^(-6)	termination condition of inner loop    
    self.itr_cg = 0		#conjugate gradient iterations completed
    self.itr_gs = 0		#golden section iterations completed
    self.itr_sumt = 0		#sumt iterations completed
    self.itr_al = 0
    self.total_itr_cg = 0	#total conjugate gradient iterations completed from doing SUMT
    self.u = 0			#line search direction
    self.max_gs = 4000
    self.max_cg = 2000
    self.max_sumt = 3
    self.max_al = 20
    self.history_ctr = []
    self.contsraints_defined = False
    self.rho = 1
    self.c = 10
    self.tolerance_feasible = 1e-4
  
    self.x.append(0)	#keep first index an open value for mathematical readiblity 
    for i in range (1,self.n_dim+1):
      self.x.append(Symbol('x[%d]'%i))
      
  def defineTheoreticalSol (self, theoretical_solution):
    self.theoretical_solution = theoretical_solution #array pointer could be error (works so far)      

  def defineStartingPosition (self, start):
    self.start = start
    self.x_val[0] = start
    
  def defineFunction (self, function):
    self.original_function = function
    self.function = function
    self.getGradientVector()

  def defineGoldenSection (self,A,B):
    self.A = A
    self.B = B
    
  def defineConvergenceTolerance (self, e1, e2, e_gs):
    self.epsilon1 = e1					#1st termination condition of steepest descent (outer loop)
    self.epsilon2 = e2					#2nd termination condition of steepest descent (outer loop)
    self.epsilon_golden_section = e_gs			#termination condition of inner loop

  def defineMaxIterations (self, i1, i2, i3):
    self.max_sumt = i1
    self.max_cg = i2
    self.max_gs = i3
    
  def defineConjugateGradientDirection (self, dir):
    self.direction_type = dir
    
  def addEqualityConstraint (self, h):
    self.h.append(h)
    self.h_boolean.append(True)
    self.lambda_eq.append(0)
    self.contsraints_defined = 1
    
  def addInequalityConstraint (self,g):
    self.g.append(g)
    self.g_boolean.append(True)
    self.lambda_ineq.append(0)
    self.contsraints_defined = 1

  def getString (self):
    string = "Function name: " + self.name + "\n"
    print string
    print self.original_function
    
  def getStringStartPos(self):
    tmp = "[ "
    for i in range (0, self.n_dim):
      if i == self.n_dim-1:
	tmp = tmp + "%4.2f"%(self.start[i])
      else:
	tmp = tmp + "%4.2f, "%(self.start[i])
    
    tmp = tmp + " ]"
    return tmp
  
  def getStringTheoSol(self):
    tmp = "[ "
    for i in range (0, self.n_dim):
      if i == self.n_dim-1:
	tmp = tmp + "%4.2f"%(self.theoretical_solution[i])
      else:
	tmp = tmp + "%4.2f, "%(self.theoretical_solution[i])
    
    tmp = tmp + " ]"
    return tmp
    
  def getStringEndSol(self):
    tmp = "[ "
    for i in range (0, self.n_dim):
      if i == self.n_dim-1:
	if self.do_al == True:
	  if self.itr_al > 0:
	    tmp = tmp + "%4.4f"%(self.uncomplete_x_val[self.itr_al][self.itr_cg][i])
	  else:
	    tmp = tmp + "%4.4f"%(self.uncomplete_x_val[self.itr_cg][i])	#BEFORE SUMT	  
	else:
	  if self.itr_sumt > 0:
	    tmp = tmp + "%4.4f"%(self.x_val[self.itr_sumt][self.itr_cg][i])
	  else:
	    tmp = tmp + "%4.4f"%(self.x_val[self.itr_cg][i])	#BEFORE SUMT
      else:
	if self.do_al == True:
	  if self.itr_al > 0:
	    tmp = tmp + "%4.4f, "%(self.uncomplete_x_val[self.itr_al][self.itr_cg][i])
	  else:
	    tmp = tmp + "%4.4f, "%(self.uncomplete_x_val[self.itr_cg][i])	#BEFORE SUMT	  
	else:
	  if self.itr_sumt > 0:
	    tmp = tmp + "%4.4f, "%(self.x_val[self.itr_sumt][self.itr_cg][i])
	  else:
	    tmp = tmp + "%4.4f, "%(self.x_val[self.itr_cg][i])	#BEFORE SUMT
	
    
    tmp = tmp + " ]"
    return tmp
  
  def getStringConstraints(self):
    print "Inequality constraints:"
    print self.g
    print "Equality constraints:"
    print self.h
    return
  
  #this method calculates the new algebraic gradient vector from self.function, but be aware that this will also update the current self.gradient_vector array
  def getGradientVector (self):
    for i in range (1,self.n_dim+1):
      self.gradient_vector[i-1] = self.function.diff(self.x[i],1)		#hierdie lyntjie kode verander foutiewelik ook self.x_val se waarde!!
      
    return self.gradient_vector
    
  def getGradientVectorAtPoint(self,point_vector):
    if self.penalties == True: 	#this means that we are either going to do do SUMT or AL, both which will affect the gradient function after each iteration
      self.getGradientVector()	#thus it is necessary to update the gradient before substituting a point into the algebraic gradient function
    
    #then we can continue to use the gradient vector sympy object to substitute the given point. This saves computation time.
    temp = []
    
    for j in range (0,self.n_dim):								#j denotes gradient_vector_at_point row number
      temp.append(self.gradient_vector[j])							#COPY element by element
      for k in range (0,self.n_dim):								#k denotes which variable (x[1],x[2],..) is to be substituted from the symbolic equation
	temp[j] = temp[j].subs(self.x[k+1],point_vector[k])					#can only substitute one variable at a time
      temp[j] = pl.np.float(temp[j]) 								#very important to ensure values are converted from sympy type values to numpy float values
  
    return pl.np.array(temp)
  
  #get f(x)
  def getCost (self,point_vector,ignore_penalties):
    if ignore_penalties == False:
      if self.penalties == True:
	if self.do_sumt == True:
	  return self.getPenaltyFunctionAtPoint(point_vector)
	elif self.do_al == True:
	  return self.getAugmentedLangrangianAtPoint(point_vector)
    
    temp = self.original_function
    
    for k in range (0,self.n_dim):
      temp = temp.subs(self.x[k+1],point_vector[k])
	
    return pl.np.float(temp)
  
  #this method returns the Penalty function with rho already substituted in. All that remains to be substituted are the primal variables itself (x)
  def getPenaltyFunction(self):
    if (self.contsraints_defined == False):
      print "No constraints were defined."
      return    
    
    terms1 = 0
    terms2 = 0
    pw = []
    
    for j in range (0, pl.np.size(self.h)):	#for each equality constraint
      terms1 = terms1 + self.h[j]**2
    terms1 = terms1*self.rho
    
    for j in range (0, pl.np.size(self.g)):	#for each inequality constraint
      pw.append(Piecewise( (0, self.g[j] <= 0), (self.rho, self.g[j] > 0)))
  
    for j in range (0, pl.np.size(self.g)):	#for each inequality constraint
      terms2 = terms2 + pw[j]*self.g[j]**2
  
    return self.original_function + terms1 + terms2

  #this method returns the Augmented Langrangian function with every parameter such as lambda_eq,lamda_ineq and rho already substituted in. All that remains to be substituted are the primal variables itself (x)
  def getAugmentedLangrangianFunction(self,lambda_eq,lambda_ineq,rho):
    if (self.contsraints_defined == False):
      print "No constraints were defined."
      return    
    
    term_penalty_eq = 0
    term_al_eq = 0
    term_ineq = 0
    pw = []
    
    #terms for augmented langriangian and penalty of equality constraint
    for j in range (0, pl.np.size(self.h)):	#for each equality constraint
      term_penalty_eq = term_penalty_eq + self.h[j]**2
      term_al_eq = term_al_eq + lambda_eq[j]*self.h[j]
    term_penalty_eq = term_penalty_eq*rho
    
    #construct max(a,0) and get inequality terms
    for j in range (0, pl.np.size(self.g)):	#for each inequality constraint
      temp = (lambda_ineq[j]/(2*rho)) + self.g[j]
      pw.append(Piecewise( (0, temp <= 0), ( temp**2, temp > 0)))
      term_ineq = term_ineq + rho*pw[j]
  
    
    return self.original_function + term_penalty_eq + term_al_eq + term_ineq

  def getPenaltyFunctionAtPoint (self,point_v):
    return self.substituteVectorIntoFunction(point_v,self.getPenaltyFunction())

  def getAugmentedLangrangianAtPoint(self,point_v):
    return self.substituteVectorIntoFunction(point_v,self.getAugmentedLangrangianFunction(self.lambda_eq,self.lambda_ineq,self.rho))
  
  def substituteVectorIntoFunction(self,point_vector,g):
    temp = g
    
    for k in range (0,self.n_dim):
      temp = temp.subs(self.x[k+1],point_vector[k])
	
    return pl.np.float(temp)

  def getFeasibility(self,vector):
    return_what = True
    for j in range(0,pl.np.size(self.h)):	#equalities
      temp = self.substituteVectorIntoFunction(vector,self.h[j])
	
      #if temp < self.tolerance_feasible and temp > -self.tolerance_feasible:	#stelling mag dalk problematies wees (was voorheen temp != 0) DINK NIE DIT WERK SO MOOI NIE
      if pl.allclose(temp, 0, self.tolerance_feasible, atol=self.tolerance_feasible) == False: #HIERDIE STELLING WERK BAIE GOED!
	self.h_boolean[j] = False
	return_what = False
      else:
	self.h_boolean[j] = True
	
    for j in range(0,pl.np.size(self.g)):	#inequalities
      temp = self.substituteVectorIntoFunction(vector,self.g[j])
	
      if temp > self.tolerance_feasible:	#stelling mag dalk problematies wees (was voorheen temp > 0) DIT WERK
	self.g_boolean[j] = False
	return_what = False
      else:
	self.g_boolean[j] = True
	
    return return_what
  
  #The purpose of this method is to construct the final solution array, thereby collecting all the solutions generated until thus far, and let self.x_val point to all the collective data
  def constructSolutionArray(self):
    if self.itr_sumt > 0:
      #self.uncomplete_x_val.append(self.x_val)
      self.x_val = self.uncomplete_x_val
 
  #do Golden Section Method
  def doGoldenSection(self):
    A_tmp = self.A
    B_tmp = self.B
    L = B_tmp - A_tmp			#Length of line, search is conducted on
    solution = 0
    m = 0
    lambda1 = A_tmp + pl.np.power(r,2)*L
    lambda2 = A_tmp + r*L
    solution_found = False
    order = 0
    #lambda can only assume positive values because we a_sol[i]lready chose a direction "u" to minimize in.
  
    while(solution_found == False):
      self.itr_gs = self.itr_gs + 1
      
      x1 = self.x_val[self.itr_cg] + lambda1*self.u		#x1 is a vector
      x2 = self.x_val[self.itr_cg] + lambda2*self.u		#x2 is a vector
    
      #This is a very important piece of code, otherwise x1 and x2 will remain a sympy object in a very large uncompressed algebraic form
      for p in range(0,self.n_dim):
	x1[p] = pl.np.float(x1[p])
	x2[p] = pl.np.float(x2[p])
    
      g1 = self.getCost(x1,False)	#get complete cost (includes original cost + penalties + Langrangian)
      g2 = self.getCost(x2,False)	#get complete cost (includes original cost + penalties + Langrangian)
  
      m = m + 1

      if L < self.epsilon_gs or self.itr_gs==self.max_gs:					
	solution = (B_tmp+A_tmp)/2	
	solution_found = True
	return solution

      if(StrictGreaterThan(g1,g2)):  
	A_tmp = lambda1
	lambda1 = lambda2
	L = B_tmp - A_tmp
	lambda2 = A_tmp + r*L

      elif(StrictLessThan(g1,g2)):  
	B_tmp = lambda2
	lambda2 = lambda1
	L = B_tmp - A_tmp
	lambda1 = A_tmp + pl.np.power(r,2)*L
	
      else:	#this else statement is necessary when g1 and g2 are essentially the same up to some decimal point which the variables cannot resemble, thus an exit for such a case is needed
	solution = (B_tmp+A_tmp)/2	
	solution_found = True
	return solution
	
  #do Conjugate Gradient Method
  def doConjugateGradient(self,direction_type):
      self.direction_type = direction_type
      solution_found = False
      

      self.f_val.append(self.getCost(self.x_val[self.itr_cg],True))	#we only want to graph how f(x) itself is improved

      while (solution_found == False):
	self.total_itr_cg = self.total_itr_cg + 1
	
	#reset direction after n+1 iterations (because then we know that this function is not quadratic and that the previous information is probably irrelevant and confusing)
	#so then we use the steepest descent direction and thereafter again the conjugate gradient direction
	#if self.itr_cg == self.n_dim + 1:
	  #self.beta = 1	#what is the purpose of this
	  #self.gradient_vector_at_point_previous = self.gradient_vector_at_point
	  #self.gradient_vector_at_point = self.getGradientVectorAtPoint(self.x_val[self.itr_cg])
	  ##self.u = -self.gradient_vector_at_point
	  #self.u = -(self.gradient_vector_at_point) + beta*self.u
	
	#elif self.itr_cg == 0:
	if self.itr_cg == 0:
	  self.gradient_vector_at_point = self.getGradientVectorAtPoint(self.x_val[self.itr_cg])
	  self.u = -self.gradient_vector_at_point

	else:
	  self.gradient_vector_at_point_previous = self.gradient_vector_at_point
	  self.gradient_vector_at_point = self.getGradientVectorAtPoint(self.x_val[self.itr_cg])		#not needed to do this assignment because it actually happens already within the function
    
	  if direction_type == 1:
	    beta = pl.np.power(getAbsoluteOfVector(self.gradient_vector_at_point),2)/pl.np.power(getAbsoluteOfVector(self.gradient_vector_at_point_previous),2)
	    
	  if direction_type > 1:
	    beta = pl.np.dot(self.gradient_vector_at_point, (self.gradient_vector_at_point - self.gradient_vector_at_point_previous))/pl.np.power(getAbsoluteOfVector(self.gradient_vector_at_point_previous),2)	#very important: dot() calculates dot product which is thr matrix multiplication equavalent for this array object type (* is only element-wise multiplication for array type). If matrix objects were used, * would already refer to matrix multiplication
	    
	    if direction_type == 3:
	      beta = max(0,beta)
	
	  self.u = -(self.gradient_vector_at_point) + beta*self.u
	
	#do a line search minimization
	lambda_i = self.doGoldenSection()
  
	#set new self.x_val value according to the minimum found on the straight line
	self.itr_cg = self.itr_cg + 1

	self.x_val.append(pl.np.array(self.x_val[self.itr_cg-1] + lambda_i*self.u))			#BEFORE SUMT
  
	for p in range(0,self.n_dim):
	  self.x_val[self.itr_cg][p] = pl.np.float(self.x_val[self.itr_cg][p])	#This is a very important piece of code, otherwise x_sol will remain sympy object in a very large uncompressed algebraic form
  
	#the following if statement used to contain an OR statement
	if (getAbsoluteOfVector(self.x_val[self.itr_cg]-self.x_val[self.itr_cg-1]).evalf() < self.epsilon1) or (getAbsoluteOfVector(self.getGradientVectorAtPoint(self.x_val[self.itr_cg])) < self.epsilon2):
	  solution_found = True
	  self.history_ctr.append(self.itr_cg)
	  self.f_val_outer.append(self.getCost(self.x_val[self.itr_cg],True))	#we only want to graph how f(x) itself is improved
	  self.feasible_graph.append(self.getFeasibility(self.x_val[self.itr_cg]))

	elif self.itr_cg==self.max_cg:
	  solution_found = True
	  self.history_ctr.append(self.itr_cg)
	  self.f_val_outer.append(self.getCost(self.x_val[self.itr_cg],True))	#we only want to graph how f(x) itself is improved
	  self.feasible_graph.append(self.getFeasibility(self.x_val[self.itr_cg]))
  
	self.f_val.append(self.getCost(self.x_val[self.itr_cg],True))	#we only want to graph how f(x) itself is improved
	
	#print-out of present outer iteration without using a newline
	if (self.do_sumt == True):
	  continue#sys.stdout.write("SUMT iteration: %3d   Conjugate gradient iteration: %3d\r" %(self.itr_sumt,self.itr_cg))
	elif (self.do_al == True):
	  sys.stdout.write("Augmented Langrangian: %d   Conjugate gradient iteration: %d\r" %(self.itr_al,self.itr_cg))
	  
	sys.stdout.flush()
   	

  def doSUMT(self):
    if (self.contsraints_defined == False):
      print "No constraints were defined."
      return
    
    self.do_sumt = True
    self.penalties = True
    solution_found = False
    
    self.f_val_outer.append(self.getCost(self.x_val[self.itr_cg],True))	#we only want to graph how f(x) itself is improved (and this first value is the f(x_start) value)
    self.feasible_graph.append(self.getFeasibility(self.x_val[self.itr_cg]))
    
    while (solution_found == False and self.max_sumt > 0):
      if self.itr_sumt > 0:
	#reset self.x_val to its initial structure (but with the first value being the latest solution)
	self.x_val = []
	self.x_val.append([])
	for i in range (0, self.n_dim):
	  self.x_val[0].append(0)
	  self.x_val[0][i] = self.uncomplete_x_val[self.itr_sumt-1][self.history_ctr[self.itr_sumt-1]][i]	#let the new first value be the latest solution found from previous SUMT iteration
	  #self.x_val[0][i] = self.start[i]	#leave this commented code here, good for illustrative purposes how SUMT is effective

      
      self.function = self.getPenaltyFunction()
      
      self.doConjugateGradient(self.direction_type)
      print self.itr_cg
      
      self.uncomplete_x_val.append(self.x_val)
      
      
      if self.itr_sumt > 0:	#the following if statement used to have an OR in.
	if (getAbsoluteOfVector(self.uncomplete_x_val[self.itr_sumt][self.itr_cg] - self.uncomplete_x_val[self.itr_sumt-1][self.history_ctr[self.itr_sumt-1]]) < self.epsilon1) or (pl.np.abs(self.substituteVectorIntoFunction(self.uncomplete_x_val[self.itr_sumt-1][self.history_ctr[self.itr_sumt-1]],self.function) - self.substituteVectorIntoFunction(self.uncomplete_x_val[self.itr_sumt][self.itr_cg],self.function)) < self.epsilon2):
	  sys.stdout.write("SUMT iteration: %3d   Conjugate gradient iteration %d\r" %(self.itr_sumt,self.itr_cg))
	  sys.stdout.write("\nSUMT termination criteria reached.")
	  sys.stdout.write("\nFeasibility: %r" %(self.getFeasibility(self.uncomplete_x_val[self.itr_sumt][self.itr_cg])))
	  #print self.itr_sumt
	  return
    
	else:
	  self.rho = self.c*self.rho

      if self.itr_sumt>=self.max_sumt-1:
	solution_found = True
	sys.stdout.write("SUMT iteration: %3d   Conjugate gradient iteration %d\r" %(self.itr_sumt,self.itr_cg))
	sys.stdout.write("\nMax SUMT iterations reached.")
	sys.stdout.write("\nFeasibility: %r" %(self.getFeasibility(self.uncomplete_x_val[self.itr_sumt][self.itr_cg])))
	return
      
      else:
	self.itr_cg = 0
	self.itr_sumt = self.itr_sumt + 1
      
  def doAugmentedLangrangian(self):
    if (self.contsraints_defined == False):
      print "No constraints were defined."
      return    

    self.do_al = True
    self.penalties = True
    self.rho = 1
    solution_found = False
    
    self.f_val_outer.append(self.getCost(self.x_val[self.itr_cg],True))	#we only want to graph how f(x) itself is improved (and this first value is the f(x_start) value)
    self.feasible_graph.append(self.getFeasibility(self.x_val[self.itr_cg]))
    
    while (solution_found == False and self.max_al > 0):
      if self.itr_al > 0:
	#reset self.x_val to its initial structure (but with the first value being the latest solution) so that conjugate gradient can fill it up with info
	self.x_val = []
	self.x_val.append([])
	for i in range (0, self.n_dim):
	  self.x_val[0].append(0)
	  self.x_val[0][i] = self.uncomplete_x_val[self.itr_al-1][self.history_ctr[self.itr_al-1]][i]	#let the new first value be the latest solution found from previous SUMT iteration
	  #self.x_val[0][i] = self.start[i]	#uncomment this line if it is desired to start again from initial starting position
      
      self.function = self.getAugmentedLangrangianFunction(self.lambda_eq,self.lambda_ineq,self.rho)	#Augmented Langrangian
      
      self.doConjugateGradient(self.direction_type)
      
      self.uncomplete_x_val.append(self.x_val)      

      
      if self.itr_al > 0:	#the following if statement used to have an OR in.
	if (getAbsoluteOfVector(self.uncomplete_x_val[self.itr_al][self.itr_cg] - self.uncomplete_x_val[self.itr_al-1][self.history_ctr[self.itr_al-1]]) < self.epsilon1) or (pl.np.abs(self.getAugmentedLangrangianAtPoint(self.uncomplete_x_val[self.itr_al-1][self.history_ctr[self.itr_al-1]]) - self.getAugmentedLangrangianAtPoint(self.uncomplete_x_val[self.itr_al][self.itr_cg])) < self.epsilon2):
	  sys.stdout.write("Augmented Langrangian iteration: %3d   Conjugate gradient iteration %d\r" %(self.itr_al,self.itr_cg))
	  sys.stdout.write("\nAugmented Langrangian termination criteria reached.")
	  sys.stdout.write("\nFeasibility: %r" %(self.getFeasibility(self.uncomplete_x_val[self.itr_al][self.itr_cg])))
	  #print self.itr_al
	  return
      
      if self.itr_al>=self.max_al-1:
	solution_found = True
	sys.stdout.write("Augmented Langrangian iteration: %d   Conjugate gradient iteration %d\r" %(self.itr_al,self.itr_cg))
	sys.stdout.write("\nMax Augmented Langrangian iterations reached.")
	sys.stdout.write("\nFeasibility: %r" %(self.getFeasibility(self.uncomplete_x_val[self.itr_al][self.itr_cg])))
	return
      
      else:
	#update approximations for Langrangian variables
	for i in range (0, pl.np.size(self.h)):
	  self.lambda_eq[i] = self.lambda_eq[i] + 2*self.rho*self.substituteVectorIntoFunction(self.uncomplete_x_val[self.itr_al][self.itr_cg],self.h[i])
	for i in range (0, pl.np.size(self.g)):
	  self.lambda_ineq[i] = self.lambda_ineq[i] + 2*self.rho*self.substituteVectorIntoFunction(self.uncomplete_x_val[self.itr_al][self.itr_cg],self.g[i])
	  
	self.itr_cg = 0
	self.itr_al = self.itr_al + 1
	self.rho = self.rho*2	#the programmer can decide on a scheme of how to increase rho
	#if self.itr_al >= 6:
	  #self.rho = 100