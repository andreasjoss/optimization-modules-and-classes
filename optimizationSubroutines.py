 #!/usr/bin/python

#Author:	Andreas Joss (16450612)
#Date:		2015/04/15 

'''
This module contains all the subroutines frequently used for optimization assignments from the sumt.py script onwards.
'''

from __future__ import division				#ensures division always returns the correct answer (eg. 1/2 = 0.5 instead of 1/2 = 0)
import argparse
import pylab as pl
from sympy import *
import mpmath as mp					#use this for mathmematical constants

'''
Global variables within this module
'''
description = "This module contains all the subroutines frequently used for optimization assignments from the sumt.py script onwards."
#n_dim = pl.np.size(vector)


'''
Functions defined by this module
'''

# Obtain absolute value of a vector
def getAbsoluteOfVector(vector):					
  sum = 0
  n_dim = pl.np.size(vector)
  
  for i in range (0,n_dim):
    sum = sum + pl.np.power(vector[i],2)
  
  return sum**Rational(1,2)			#must use sympy's rational function because sum contains a sympy_float object type


# Obtain absolute value of a gradient vector evaulated at a point
def getAbsoluteOfFirstOrderGradientVector(gradient_vector,point_vector):
  sum = 0
  k = 0
  j = 0
  n_dim = pl.np.size(point_vector)
  
  #create new list from scratch (so that modification of this list does not affect the original list contents)
  temp = []
  for j in range (0,n_dim):
    temp.append(0)
  
  for j in range (0,n_dim):
    temp[j] = gradient_vector[j] 				#copy element by element into new variable, otherwise only pointer address is copied and changes will affect both variables (undesired)
    for k in range (0,n_dim):
      temp[j] = temp[j].subs(x[k+1],point_vector[k])		#can only substitute one variable at a time

    sum = sum + pl.np.power(temp[j],2)   

  return sum**Rational(1,2)


# Substitute vector position into function g and return the evaluated function
def substituteVectorIntoFunction(vector,g):
  n_dim = pl.np.size(vector)
  temp = []
  
  for j in range (0,n_dim):
    temp.append(vector[j])
    for k in range (0,n_dim):
      temp[j] = g[j].subs(x[k+1],vector[k])		#can only substitute one variable at a time

  return temp

# Substitute a point into algebraic gradient vector
def getGradientVectorEvaluatedAtPoint(gradient_vector,point_vector):
  n_dim = pl.np.size(point_vector)
  temp = []
  
  for j in range (0,n_dim):								#j denotes gradient_vector_at_point row number
    temp.append(gradient_vector[j])							#COPY element by element
    for k in range (0,n_dim):								#k denotes which variable (x[1],x[2],..) is to be substituted from the symbolic equation
      temp[j] = temp[j].subs(x[k+1],point_vector[i][k])					#can only substitute one variable at a time
    temp[j] = pl.np.float(temp[j]) 							#very important to ensure values are converted from sympy type values to numpy float values
  
  return pl.np.array(temp)

# Golden Section Method
def doGoldenSection(a,b):
  L = b - a			#Length of line, search is conducted on
  solution = pl.np.zeros((3,1))
  m = 0
  lambda1 = a + pl.np.power(r,2)*L
  lambda2 = a + r*L
  solution_found = False
  order = 0
  global inner_iterations
  inner_iterations = 0 
  #lambda can only assume positive values because we a_sol[i]lready chose a direction "u" to minimize in.
  
  while(solution_found == False):
    global inner_iterations
    inner_iterations = inner_iterations + 1
    
    x1 = x_sol[i] + lambda1*u		#x1 is a vector
    x2 = x_sol[i] + lambda2*u		#x2 is a vector
    
    #This is a very important piece of code, otherwise x1 and x2 will remain a sympy object in a very large uncompressed algebraic form
    for p in range(0,n_dim):
      x1[p] = pl.np.float(x1[p])
      x2[p] = pl.np.float(x2[p])
    
    g1 = f					#f is a function of multi variables
    g2 = f

    for p in range (0,n_dim):
      g1 = g1.subs(x[p+1],x1[p])
      g2 = g2.subs(x[p+1],x2[p])	
  
    m = m + 1

    if L < epsilon_golden_section:					
      solution[0] = (b+a)/2	
      solution[2] = m
      solution_found = True
      return solution

    if(StrictGreaterThan(g1,g2)):  
      a = lambda1
      lambda1 = lambda2
      L = b - a
      lambda2 = a + r*L

    elif(StrictLessThan(g1,g2)):  
      b = lambda2
      lambda2 = lambda1
      L = b - a
      lambda1 = a + pl.np.power(r,2)*L